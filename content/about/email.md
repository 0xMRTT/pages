---
title: "Email"
type: page
---

My email address is my username + `@` + `proton` + `.` + `me`. If you can, please use PGP with [my public key](https://git.exozy.me/0xMRTT.gpg).
