---
title: "Projects"
type: page
---


### Projects

#### GNOME

- [Gradience](/projects/gradience/)
- [Bavarder](/projects/bavarder/)
- [Imaginer](/projects/imaginer/)

#### DevOps