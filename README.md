# 0xMRTT's website

[![status-badge](https://ci.exozy.me/api/badges/0xmrtt/website/status.svg)](https://ci.exozy.me/0xmrtt/website)

## Dev

### Setup

Make sure you have git and hugo installed

``` shell
git clone --recursvie forgejo@git.exozy.me:0xmrtt/website.git
cd website
git submodule update
```

### Run

```shell
hugo server
```